import rsa
from binascii import b2a_hex, a2b_hex
import datetime
import argparse
from uuid import getnode as get_mac
import netifaces

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g','--genkey', action="store_true" ,help='generate new rsa publib key and private key')
    parser.add_argument('-d','--due', type=str,help='license due day yyyymmdd')
    parser.add_argument('-m','--mac', type=str,help='target mac address')
    parser.add_argument('-v','--valid_text', type=str, help='valid encrypted text')
    #parser.add_argument('-e','--encrypt', action="store_true" ,help='encrypt mac and due to encrypt text')
    return parser.parse_args()
def get_mac_hex():
    return str(hex(get_mac())[2:])

def get_all_device_mac():
    mac_list = []
    for i in netifaces.interfaces():
        mac_buffer = netifaces.ifaddresses(i)[netifaces.AF_PACKET][0]['addr']
        if mac_buffer != '00:00:00:00:00:00':
            mac_list.append(mac_buffer.replace(":",""))
    return mac_list


class rsacrypt():
  
    @classmethod
    def gen_key(cls,digi=256):
        pubkey, prikey = rsa.newkeys(digi)
        print(pubkey, prikey)
        return pubkey.save_pkcs1(), prikey.save_pkcs1()
    def __init__(self, pubkey = None, prikey = None):
        if pubkey :
            self.pubkey = rsa.PublicKey.load_pkcs1(pubkey)
        if prikey :
            self.prikey = rsa.PrivateKey.load_pkcs1(prikey)


    def encrypt(self, text):
        self.ciphertext = rsa.encrypt(text.encode(), self.pubkey)
        return b2a_hex(self.ciphertext)

    def print_mac_duedate(self, enctext):
        decrypt_text = self.decrypt(enctext).decode()
        return str(decrypt_text[:-8]),str(decrypt_text[-8:])
    
    def check_valid(self, enctext):
        try:
            decrypt_text = self.decrypt(enctext).decode()
            if str(decrypt_text[:-8]) in str(get_all_device_mac()):
                valid_date = datetime.datetime.strptime(decrypt_text[-8:], "%Y%m%d")
                if (valid_date + datetime.timedelta(days=1) - datetime.datetime.now()).total_seconds() > 0 :
                    return True
                else:
                    return False
            else:
                return False
        except:
            return False

    def decrypt(self, text):
        decrypt_text = rsa.decrypt(a2b_hex(text), self.prikey)
        return decrypt_text

if __name__ == '__main__':

    args = get_args()
    rs_obj = rsacrypt()
    if args.genkey:
        pubkey,prikey = rs_obj.gen_key(256)
        with open("pub.key",'wb') as f:
            f.write(pubkey)
        with open("pri.key",'wb') as f:
            f.write(prikey)
    if args.due:
        pubkey = open("pub.key",'rb').read()
        prikey = open("pri.key",'rb').read()
        due = args.due
        if args.mac:
            args.mac = args.mac.replace(":","").lower()
            #mac_int = int(args.mac.replace(":",""),16)
            text = str(args.mac) + str(due)
        else:
            text = get_mac_hex() + str(due)

        rs_obj = rsacrypt(pubkey=pubkey,prikey=prikey)
        encrypt_text = rs_obj.encrypt(text)
        decrypt_text = rs_obj.decrypt(encrypt_text).decode()
        print("original:\n{}".format(text))
        print("encrypt:\n{}".format(encrypt_text.decode()))
        print("decrypt:\n{}".format(decrypt_text))
        print("================")
    if args.valid_text:
        #pubkey = open("pub.key",'rb').read()
        prikey = open("pri.key",'rb').read()
        rs_obj = rsacrypt(prikey=prikey)
        decrypt_text = rs_obj.decrypt(args.valid_text.encode()).decode()
        valid_result = rs_obj.check_valid(args.valid_text.encode())
        print("input license:\n{}".format(args.valid_text))
        print("system mac: {}".format(get_all_device_mac()))
        print("system date: {}".format(datetime.date.today().strftime('%Y%m%d')))
        print("licnese mac: {}\ndue date: {}".format(*rs_obj.print_mac_duedate(args.valid_text.encode())))
        print("license valid: {}".format(valid_result))
        print("================")
    


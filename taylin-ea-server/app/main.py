from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.middleware.cors import CORSMiddleware
# from fastapi.staticfiles import StaticFiles
import uvicorn
import json
import os
from encrypt_mac_due import rsacrypt, get_all_device_mac
import logging
import datetime

app = FastAPI()

origins = [
   "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/api/v1/online")
async def online():
    online_list = manager.getOnlineList()
    output = {
        'online': online_list
    }
    return output

"""
    WebSocket
"""
class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []
        self.userWebsocket = {}
        self.userData = {}

    async def connect(self, websocket: WebSocket, client_id, device_type, display_name, roomId, publishId):
        await websocket.accept()
        self.active_connections.append(websocket)
        self.userWebsocket[client_id] = websocket
        self.userData[client_id] = {
            "display_name": display_name,
            "device_type": device_type,
            "roomId": roomId,
            "publishId": publishId
        }

    def disconnect(self, websocket: WebSocket, client_id):
        self.active_connections.remove(websocket)
        del self.userWebsocket[client_id]
        del self.userData[client_id]
        
    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)
    
    async def send_personal_id_message(self, message: str, receiver_id, sender_id):
        websocket = self.userWebsocket[receiver_id]
        if websocket == None:
            return
        output = {
            'type': "message",
            'sender_id': sender_id,
            'sender_name': self.userData[sender_id]['display_name'],
            'sender_type': self.userData[sender_id]['device_type'],
            "value": message
        }
        await websocket.send_text(json.dumps(output))
    
    def getOnlineList(self):
        output = []
        for key, value in self.userWebsocket.items():
            output.append({
                'id': key,
                'device_type': self.userData[key]["device_type"],
                'display_name': self.userData[key]["display_name"],
                'roomId': self.userData[key]["roomId"],
                'publishId': self.userData[key]["publishId"]
            })
        return output

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)


manager = ConnectionManager()

@app.websocket("/ws/{device_type}/{client_id}/{display_name}/{roomId}/{publishId}")
async def websocket_endpoint(websocket: WebSocket, device_type: str, client_id: str, display_name: str, roomId="", publishId=""):
    await manager.connect(websocket, client_id, device_type, display_name, roomId, publishId)
    try:
        while True:
            data = await websocket.receive_text()
            # print(data)
            try:
                msg = json.loads(data)
                if 'type' in msg:
                    if msg['type'] == 'online':
                        pass
                    elif msg['type'] == 'message':
                        # print(f"Receive from Client #{client_id} send_to: {msg['receiver']} value: {msg['value']}")
                        await manager.send_personal_id_message(msg['value'], msg['receiver'], client_id)
                else:
                    await manager.send_personal_id_message(msg['content'], msg['send_to'])
            except Exception as e:
                await manager.send_personal_message(f"Something fail {e}", websocket)
    except WebSocketDisconnect:
        manager.disconnect(websocket, client_id)
        # print(f"Client #{client_id} left the chat")
    
@app.on_event("shutdown")
async def shutdown_event():
    pass

if __name__ == "__main__":
    if os.getenv('TAYLIN_EA_PUBLISH')=='publish':
        logger = logging.getLogger(__name__)
        rs_obj = rsacrypt()
        prikey = b'-----BEGIN RSA PRIVATE KEY-----\nMIGrAgEAAiEAgRxt/TxNth5cPP0pYni3ZhuR5yO4DrG+1ZrHyryJ3OECAwEAAQIg\nHDsVKbwX2eyFiqhodimi9a3R7epBpF0qgsF4JoOPwIkCEgDDYVakF42SNkfDEy8O\nz+IOwwIQAKkrdoEMEEwpt+LohQkziwISALvbzdi9JMxfEKUKvwZ475zNAg8YFgMa\nXpBXd70hxn9ijv8CETRRySvevq7zGdEXW8alJlBH\n-----END RSA PRIVATE KEY-----\n'
        # prikey = b'-----BEGIN RSA PRIVATE KEY-----\nMIGrAgEAAiEAqEb0Q6cdg52vv8d4WGw+d4uML3noReQgmrizb2VSEskCAwEAAQIg\nchOHy8UikPL5Db2fq/HU9npYy3vO+U0Bi2AbUQLdKzECEgC9wF43PrZkTmWyH3c/\nie45ewIQAOMHQWF4Uob6Ye0Z2yuHiwIRFllc/3bEdsYBFly8Z+BwDZcCD1F6zCNQ\nTMY23Kh87CUuSwISAIHpQfTBwZSHoKAvh5UQSzvn\n-----END RSA PRIVATE KEY-----'
        rs_obj = rsacrypt(prikey=prikey)
        air_license = os.environ["TAYLIN_EA_LICENSE"]
        valid_result = rs_obj.check_valid(air_license.encode())
        logger.warning("input license:\n{}".format(air_license))
        logger.warning("current mac: {}".format(get_all_device_mac()))
        logger.warning("current date: {}".format(datetime.date.today().strftime('%Y%m%d')))
        logger.warning("licnese mac: {}\ndue date: {}".format(*rs_obj.print_mac_duedate(air_license.encode())))
        logger.warning("license valid: {}".format(valid_result))

        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'aircc.settings')
        if valid_result:
            uvicorn.run(app, host="0.0.0.0", port=5000)
        else:
            logger.info("invalide license")
    else:
        uvicorn.run(app, host="0.0.0.0", port=5000)
import { isProduction, status, setOnlineList } from "./manager.js";
import * as api from "./api.js";
import * as modules from "./modules.js";
import * as draw from "./draw.js";

const init = async () => {
  // initialize
  modules.initSettingModal();
  draw.init();
  // Get the online list
  let onlineURL = isProduction
    ? `https://${status.url}/f/api/v1/online`
    : `http://${status.url}:5000/api/v1/online`;
  let data = await api.getOnlineList(onlineURL);
  setOnlineList(data);
  modules.routinelyUpdateOnlineList();
  console.log(status.online);
  modules.drawOnlineList(status.online);
  modules.handleOnlineListOnClick(status.online, status.callState);
};

navigator.mediaDevices
  .getUserMedia({ audio: true })
  .then(async function (stream) {
    console.log("got mic connection");
    console.log(await getAudioDeviceLabel(stream))
    init();
  })
  .catch(function (err) {
    console.log("No mic for you!");
  });



async function getAudioDeviceLabel(stream) {
  let audioDeviceLabel = 'unknown'
  const tracks = stream.getAudioTracks()
  if( tracks && tracks.length >= 1 && tracks[0] ) {
    const settings = tracks[0].getSettings()
    const chosenDeviceId = settings.deviceId
    if (chosenDeviceId) {
      let deviceList = await navigator.mediaDevices.enumerateDevices()
      deviceList = deviceList.filter(device => device.deviceId === chosenDeviceId)
      if (deviceList && deviceList.length >= 1) audioDeviceLabel = deviceList[0].label
    }
  }
  return audioDeviceLabel 
}